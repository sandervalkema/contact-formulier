const express = require('express'),
      app = express(),
      port = 8070;

app.use(express.static('public')); 
app.listen(port);

console.info(`Server is running on port ${port}. Check url: http://localhost:${port}/`);