const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const CopyPlugin = require('copy-webpack-plugin');

// default configuration
let config = {
    entry: [
        './assets/scss/index.scss',
        './assets/vue/index.ts',
    ],
    plugins: [
        new VueLoaderPlugin(),
        new MiniCssExtractPlugin({ filename: 'bundle.css' }),
        new CopyPlugin({
            patterns: [
                {
                    from: './assets/img',
                    to: 'img'
                },
                {
                    from: './assets/fonts',
                    to: 'fonts'
                }
            ]
        }),
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    appendTsSuffixTo: [/\.vue$/],
                },
                exclude: '/node_modules'
            },
            {
                test: /\.vue$/,
                use: 'vue-loader',
                exclude: '/node_modules'
            },
            {
                test: /\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: false
                        }
                    },
                    "sass-loader"
                ],
                exclude: '/node_modules'
            }
        ]
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public'),
    },
    resolve: {
        extensions: ['', '.vue', '.json', '.js', '.jsx', '.ts', '.tsx'],
        alias: {
            '#vue': path.resolve(__dirname, 'assets/vue/'),
            '#ts': path.resolve(__dirname, 'assets/ts/'),
            '#json': path.resolve(__dirname, 'assets/json/')
        }
    }
};

module.exports = (env, argv) => {

    // development configuration
    if (argv.mode === 'development') {
        config.devtool = 'inline-source-map';
    }

    // production configuration
    if (argv.mode === 'production') {
        config.plugins.push(new CssMinimizerPlugin());
        config.optimization = {
            minimizer: [new CssMinimizerPlugin()]
        };
    }

    return config;
};
