# Intro

![Contact Form](/screenshot.png?raw=true)

This repository demonstrates the validation mechanics in a contact form.

Written in vue/javascript/typescript.

> **_NOTE:_**   no form data is send or posted to a database. This functionality is outside the scope of this project.

# Required Windows Software

- Node.js               Download here: https://nodejs.org/en/download
- Gitbash (optional)    Download here: https://git-scm.com/downloads

# Initialize

Run the following commands in this order:
- `npm install`
- `npm run prod-build`
- `npm run server`

Open website: http://localhost:8070/

# Update Assets

Update assets for development:
- asset building with listener:  `npm run watch`
- single asset building:         `npm run dev-build`

Update assets for production: `npm run prod-build`

# Author/Creator

Sander Valkema