// vendor
import { reactive } from "vue";

// services
import Validator from "#ts/services/validator";

// interfaces
import {
    Field as FieldInterface,
    FieldHelper as FieldHelperInterface,
} from "#ts/interfaces/field";

const validator = new Validator();

export const form = reactive({
    fields: [] as FieldHelperInterface[],
    hasRequired: false as boolean,

    /**
     * Initialize and reformat contact.json file with expanded form handles
     *
     * @param {FieldInterface[]} form - contact.json file
     */
    initialize(form: FieldInterface[]) {
        this.fields = form.map((properties: FieldInterface) => {
            const isRequired: boolean = properties?.conditions?.required ?? false;
            const hasValue: boolean = (properties?.attrs?.value ?? null) !== null;

            if (isRequired) this.hasRequired = true;

            return {
                properties,
                errors: [],
                states: {
                    isRequired,
                    hasValue,
                    isValid: false,
                    isInteracted: false,
                },
            };
        });
    },

    /**
     * Get field helper by specific field name
     *
     * @param {string} name - field name
     *
     * @throws {Error}
     *
     * @returns {FieldHelperInterface}
     */
    getFieldByName(name: string): FieldHelperInterface {
        const field = this.fields.find((field: FieldHelperInterface) => field.properties.attrs.name === name);

        if (!field) {
            throw new Error(`[ERROR] unknow field name: ${name}`);
        }

        return field;
    },

    /**
     * Set value in field with specific field name
     *
     * @param {string} name  - field name
     * @param {any}    value - field value
     */
    setValueByName(name: string, value: any) {
        let field = this.getFieldByName(name);
        const errors = validator.validate(value, field.properties.conditions);

        field.errors = errors;
        field.states.isValid = errors.length === 0;
        field.states.isInteracted = true;
        field.states.hasValue = value !== null && value !== undefined;
        field.properties.attrs.value = value;
    },

    /**
     * Check and execute validity of all fields in form and return validity state
     *
     * @returns {boolean}
     */
    isValidSubmit(): boolean {
        return this.fields.reduce((acc: boolean, field: FieldHelperInterface) => {
            const errors = validator.validate( field.properties.attrs?.value ?? null, field.properties.conditions );
            if (errors.length === 0) return acc;
            field.errors = errors;
            field.states.isValid = false;
            field.states.isInteracted = true;

            return false;
        }, true)
    }
});
