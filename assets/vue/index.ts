import { createApp } from 'vue';
import Contact from '#vue/components/layouts/contact.vue';
import Translation from '#json/translation.json';

const app = createApp({});

app.component('contact', Contact);

app.config.globalProperties = {
    $translate: Translation
}

declare module 'vue' {
    interface ComponentCustomProperties {
        $translate: any
    }
}

app.mount('#vue');