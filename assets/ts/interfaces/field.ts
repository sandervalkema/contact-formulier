import { Error } from '#ts/interfaces/validate';

export interface Field {
    is: string
    attrs: {
        name: string
        value?: any
        [key: string]: any       
    } 
    label?: string 
    conditions?: Conditions    
    options?: Options[]    
}

export interface FieldHelper {
    properties: Field,
    errors: Error[]
    states: States
}

export interface Conditions {
    required?: boolean
    regex?: string
    minString?: number
    maxString?: number
    minNumber?: number
    maxNumber?: number
    function?: Function
}

// options for select field
export interface Options {
    label: string
    value: any
}

export interface States {
    isRequired: boolean
    isValid: boolean
    isInteracted: boolean
    hasValue: boolean
}
