export class ValidationError extends Error {

    /**
     * Custom validation error
     * 
     * @param {string} message 
     */
    constructor(message: string) {
        super(message);
        Object.setPrototypeOf(this, ValidationError.prototype);
    }
}
