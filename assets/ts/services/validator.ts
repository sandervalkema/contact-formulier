import { Error } from "#ts/interfaces/validate";
import { Conditions } from "#ts/interfaces/field"; 
import { ValidationError } from "./errors";
import Translation from '#json/translation.json';

export default class Validator {

    /**
     * Error messages
     * 
     * @constant
     * @type {string}
     */
    public initialErrorsState: Error[] = [];

    /**
     * Validates field value based on predefined conditions
     * 
     * @param {any}        value      - field value
     * @param {Conditions} conditions - validation conditions/requirements
     * 
     * @returns {Error[]}
     */
    validate(value: any, conditions: Conditions = null): Error[] {
        if (!conditions) return this.initialErrorsState;

        return Object.keys(conditions).reduce((acc: Error[], type: keyof Conditions) => {
            if (!(type in conditions)) return acc;

            try {
                if (!conditions.required && !value) return acc;
                this.getErrors()[type](value, conditions[type]);
            } catch (error) {
                if (error instanceof ValidationError) return [...acc, { type, message: error.message }];
                throw error;
            }

            return acc;
        }, []);
    };

    /**
     * Validation conditions
     * 
     * @returns {Record<keyof Conditions, Function>}
     */
    getErrors(): Record<keyof Conditions, Function> {
        return {
            required: (value: any, condition: boolean): void => {                
                if (condition && (!value || value.length === 0)) {
                    throw new ValidationError(Translation.form.errors.required);
                }
            },
            minString: (value: string, condition: number): void => {
                if (!value || value.length < condition) {
                    throw new ValidationError(Translation.form.errors.minString.replace('#placeholder#', condition.toString()));
                }
            },
            maxString: (value: string, condition: number): void => {
                if (value && value.length > condition) {
                    throw new ValidationError(Translation.form.errors.maxString.replace('#placeholder#', condition.toString()));
                }
            },
            minNumber: (value: number, condition: number): void => {
                if (!value || (isNaN(value) || value < condition)) {
                    throw new ValidationError(Translation.form.errors.minNumber.replace('#placeholder#', condition.toString()));
                }
            },
            maxNumber: (value: number, condition: number): void => {
                if (value &&(isNaN(value) || value > condition)) {
                    throw new ValidationError(Translation.form.errors.maxNumber.replace('#placeholder#', condition.toString()));
                }
            },
            regex: (value: any, condition: string): void => {
                if (!(new RegExp(condition)).test(value)) {
                    throw new ValidationError(Translation.form.errors.regex);
                }
            },
            function: (value: any, condition: Function): void => condition(value)
        }
    }
}